
### Site Options

![](./journal/20170512_093222.jpg)


> Questions
* Where should I dispatch props? SiteOptions or QueryOptions?
* How do I unit test save to JSON file?


###### Add
* Type input
* Time (range) input
* Date input
* Add button

###### List
* View (type - date - startTime - endTime)


* move up
  * click to move up in list view
  * if at top, do nothing


* delete
  * deletes query


###### Actions
* ADD_OPTION_QUERY
  * { queryId, type, startTime, endTime, date }


* GET_OPTION_QUERY_LIST,
  * [] of query options


* SHIFT_QUERY_UP
  * { queryId }


* DELETE_QUERY_OPTION
  * { siteId, queryId }



### Login Time

###### Add
* Time input
* Add button

###### List times
* Radio button
* Time
* Delete button

###### Actions
* ADD_REQUEST_TIME
  * { timeString }


* REMOVE_REQUEST_TIME
  * { timeId }


* SELECT_REQUEST_TIME
  * { timeID }



## Status View

###### Time view
* Login time
* Current time

###### Log View
* Initial login...
* main request...
* Successful shifts...
  * [] of selected shifts
  * time of submit
* Success / Fail message


## Start
###### Button
* Starts app
