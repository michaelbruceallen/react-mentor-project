// Modules
import _$ from 'jquery';
import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import jsdom from 'jsdom';
import chai from 'chai';
import  chaiJquery from 'chai-jquery';

// Reducers
import reducers from '../src/reducers';

// JSDOM setup
const { JSDOM } = jsdom;
const dom = new JSDOM();


// Node global DOM setup
global.window = dom.window; // Node window environment
global.document = dom.window.document; // Node DOM
global.navigator = global.window.navigator;


// Attach jQuery to DOM
const $ = _$(global.window);


// Chai jQuery setup
const { expect } = chai;
chaiJquery(chai, chai.util, $);


$.fn.simulate = function(event, value) {
  // Set value if entered
  if (value) {
    this.val(value);
  }
  // Simulate [event]
  TestUtils.Simulate[event](this[0]);
}

// Render JSX component
function renderComponent(ComponentClass, state = {}) {

  const renderedDocument = TestUtils.renderIntoDocument(
    <Provider store={createStore(reducers, state)}>
      <ComponentClass/>
    </Provider>
  );

  const componentDOMNode = ReactDOM.findDOMNode(renderedDocument)

  // Wrap node with jQuery
  return $(componentDOMNode);
}


export { expect, renderComponent };
