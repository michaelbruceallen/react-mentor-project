import React from 'react';

// Helpers
import { expect, renderComponent } from '../test_helper';

// Components
import MainMenu from '../../src/components/MainMenu';


describe("MainMenu", () => {

  let component;

  beforeEach(() => {
    component = renderComponent(MainMenu);
  })

  it("should exist", () => {
    expect(component).to.exist;
  })

  it("shows a OptionDisplay", () => {
    expect(component.find('.options-display')).to.exist;
  });

});
