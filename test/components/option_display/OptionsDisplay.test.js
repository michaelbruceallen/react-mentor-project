// Helpers
import { expect, renderComponent } from '../../test_helper';

// Components
import OptionsDisplay from '../../../src/components/option_display/OptionDisplay';


describe("OptionsDisplay", () => {

  let component;

  beforeEach(() => {
    component = renderComponent(OptionsDisplay);
  });

  it("has AddShiftQuery", () => {
    expect(component.find('.add-shift-query')).to.exist;
  });

  it("has ViewShiftQueries", () => {
    expect(component.find('.view-shift-queries')).to.exist;
  });

  it("has AddRequestTime", () => {
    expect(component.find('.add-request-time')).to.exist;
  });

  it("has ViewRequestTimes", () => {
    expect(component.find('.view-request-times')).to.exist;
  });

});
