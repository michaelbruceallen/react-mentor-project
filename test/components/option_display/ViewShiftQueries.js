// Helpers
import { expect, renderComponent } from '../../test_helper';

// Components
import ViewShiftQueries from '../../../src/components/option_display/ViewShiftQueries';

const props = {
  shiftQueryList: [
      {
        "id": "1",
        "type": "reception",
        "date": "05/15/2017",
        "startTime": "",
        "endTime": ""
      },
      {
        "id": "2",
        "type": "breakfast",
        "date": "05/16/2017",
        "startTime": "09:30 AM",
        "endTime": ""
      },
      {
        "id": "3",
        "type": "breakfast",
        "date": "05/18/2017",
        "startTime": "",
        "endTime": "02:00 PM"
      }
    ]
  }

describe("ViewShiftQueries", () => {

  let component;

  it("exists", () => {
    component = renderComponent(ViewShiftQueries);
    expect(component).to.exist;
  });

  describe("Rendering Query List", () => {

    beforeEach(() => {
      component = renderComponent(ViewShiftQueries, props);
    });

    it("renders a 3 queries", () => {
      expect(component.find('.shift-query').length).to.equal(3)
    });

  });

});
