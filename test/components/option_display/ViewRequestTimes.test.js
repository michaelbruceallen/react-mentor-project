// Helpers
import { expect, renderComponent } from '../../test_helper';

// Components
import ViewRequestTimes from '../../../src/components/option_display/ViewRequestTimes'

describe("ViewRequestTimes", () => {

  let component, props;

  beforeEach(() => {
    props = {
      requestOptions: {
        choice: "ae736d",
        times: [
          { id: "ae736d", time: '15:00:53.000' },
          { id: "be736e", time: '02:45:30.000' },
          { id: "ce736f", time: '13:30:00.500' }
        ]
      }
    }
    component = renderComponent( ViewRequestTimes, props )
  });

  it("has .view-request-times", () => {
    expect(component)
      .to.have.class('view-request-times');
  });

  it("has form.request-time-list", () => {
    expect(component.find('form'))
      .to.have.class('request-time-list');
  });

  // REQUEST TIME FIELDS
  describe("Request Time Fields", () => {

    let requestTimeFields;

    beforeEach(() => {
      requestTimeFields = component.find('.request-time-section');
    });

    it("renders 3 request times", () => {
      expect(requestTimeFields.length)
        .to.equal(3);
    });

    describe("Selecting", () => {

      let currentCheckedId, times;

      beforeEach(() => {
        times = props.requestOptions.times;
      });

      const currentCheckedRadioId = () => {
        return component.find('.request-time-radio:checked').attr('id');
      }
      const updateComponentPropsChoice = (choiceId) => {
        props.requestOptions.choice = choiceId;
        component = renderComponent( ViewRequestTimes, props);
      }

      it("it selects choice", () => {
        updateComponentPropsChoice(times[0].id)
        currentCheckedId = currentCheckedRadioId();
        expect(currentCheckedId).to.equal(props.requestOptions.choice);

        updateComponentPropsChoice(times[1].id)
        currentCheckedId = currentCheckedRadioId();
        expect(currentCheckedId).to.equal(props.requestOptions.choice);

      });

      it("handles user select", () => {
        updateComponentPropsChoice(times[0].id);
        const radioToSelect = component.find(`.request-time-radio#${times[1].id}`);
        radioToSelect.simulate('change');
        updateComponentPropsChoice(times[1].id);
        currentCheckedId = currentCheckedRadioId();
        expect(currentCheckedId).to.equal(times[1].id);
      });

    });

    it("has button.request-time-delete", () => {
      expect(requestTimeFields.find('button.request-time-delete')).to.exist;
    });


    it("can delete", () => {
      const choice = props.requestOptions.choice;
      let deleteButton = component.find(`.request-time-delete#${choice}`);
      deleteButton.simulate('click');
      deleteButton = component.find(`.request-time-delete#${choice}`);
      expect(deleteButton).to.not.exist;

    });

  });


});
