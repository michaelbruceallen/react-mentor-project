// Modules
const path = require('path');


module.exports = {

  // Entry index.js
  entry: './src/index.js',

  // Output build/bundle.js
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  }

}
