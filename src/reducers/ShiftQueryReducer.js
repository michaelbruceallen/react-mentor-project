// Types
import { ADD_SHIFT_QUERY } from '../actions/types';
import { GET_SHIFT_QUERY_LIST } from '../actions/types';
import { DELETE_SHIFT_QUERY } from '../actions/types';
import { PRIORITIZE_SHIFT_QUERY } from '../actions/types';

export default function(state = [], action) {

  switch (action.type) {
    case GET_SHIFT_QUERY_LIST:
    case DELETE_SHIFT_QUERY:
    case ADD_SHIFT_QUERY:
    case PRIORITIZE_SHIFT_QUERY:
      return action.payload;
    default:
      return state;
  }

}
