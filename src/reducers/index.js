// Modules
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

// Reducers
import RequestOptionReducer from './RequestOptionReducer';
import ShiftQueryReducer from './ShiftQueryReducer';


const rootReducer = combineReducers({
  requestOptions: RequestOptionReducer,
  shiftQueryList: ShiftQueryReducer,
  form: formReducer,


});

export default rootReducer;
