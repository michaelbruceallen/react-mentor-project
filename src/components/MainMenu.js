// Modules
import React, { Component } from 'react';

// Components
import OptionsDisplay from  './option_display/OptionDisplay';


class MainMenu extends Component {
  render() {
    return (
      <div className="main-menu">
        <OptionsDisplay />
      </div>
    );
  };
}

export default MainMenu;
