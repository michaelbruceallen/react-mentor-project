// Modules
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

// Actions
import { addShiftQuery } from '../../actions';
import { prioritizeShiftQuery } from '../../actions';

class AddShiftQuery extends Component {

  renderField(field) {

    let {touched} = field.meta;
    // Untouched and error
    const isError = (touched && field.meta.error)
      ? true
      : false;

    let inputError = '';
    if (isError)
      inputError = 'error help is-danger';

    return (
      <div className={`field ${field.className}`}>
        <label className="label is-small">{field.label}</label>
        <p className="control">
          <input className={`input is-small ${inputError}`}
                 step={field.step}
                 type={field.type}
                 {...field.input}
                 />
        </p>
        <p className={inputError}>
          {isError
            ? field.meta.error
            : ''}
        </p>
      </div>
    )
  }

  onSubmit(values) {
    this.props.addShiftQuery('mandalay', values);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}
            className="add-shift-query"
            >
        <Field type="text"
               className="shift-type"
               label="Shift Type"
               name="type"
               component={this.renderField}
               />
        <Field type="date"
               className="shift-date"
               label="Date"
               name="date"
               component={this.renderField}
               />
        <Field type="time"
               className="shift-time"
               label="Start Time"
               name="startTime"
               step="900"
               component={this.renderField}
               />
        <Field type="time"
               className="shift-time"
               label="End Time"
               name="endTime"
               step="900"
               component={this.renderField}
               />
        <button className="shift-submit">Submit</button>
      </form>
    )
  }
}

function validate(values) {
  const errors = {};

  const timeExpression = /^[0-9]{2}:[0-9]{2}$/;
  const dateExpression = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;

  const fields = ['type', 'date', 'startTime', 'endTime']

  // Iterate fields and check if value.
  fields.forEach((field) => {
    if (!values[field])
      // Space between i.e. shift Type vs shiftType
      errors[field] = `You cannot leave ${field.replace(/([A-Z])/, ' $1')} field blank.`;
    }
  );

  // Check time format mm:ss a
  if (values.startTime && !values.startTime.match(timeExpression))
    errors.startTime = "You must use a valid time format mm:ss pm";

  // Check time format mm:ss a
  if (values.endTime && !values.endTime.match(timeExpression))
    errors.endTime = "You must use a valid time format mm:ss pm";

  // Check date format MM/DD/YYYY
  if (values.date && !values.date.match(dateExpression))
    errors.date = "You must use a valid date format MM/DD/YYYY";

  return errors;
}


function mapStateToProps(state) {
  return { shiftForm: state.shiftForm }
}

// Redux Form connection
AddShiftQuery = reduxForm({
  form: 'shiftForm',
  validate
})(AddShiftQuery)

// Redux connection
AddShiftQuery = connect(mapStateToProps, {
  addShiftQuery,
  prioritizeShiftQuery
})(AddShiftQuery);

export default AddShiftQuery;
