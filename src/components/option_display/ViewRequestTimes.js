// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import { deleteRequestTime, setRequestTimeChoice } from '../../actions';


class ViewRequestTimes extends Component {

  render() {

    const { handleSubmit } = this.props;

    return (
      <section className="view-request-times">
        <form className="request-time-list">
          {this.renderRequestTimes()}
        </form>
      </section>
    )
  }

  renderRequestTimes() {
    const { choice, times } = this.props.requestOptions;
    if (!times) {
      return (
        <div>hi</div>
      )
    }
    return times.map((time) => {
      let isChecked = '';
      if (time.id === choice) {
        isChecked = 'is-active'
      }

      return (
        <div key={time.id} className="request-time-section field">
          <p className="control">
            <label className="radio">
              <input type="radio"
                     id={time.id}
                     className="request-time-radio"
                     checked={isChecked}
                     name="question"
                     onChange={this.handleSelect.bind(this)}
                     />
              <a>{time.time}</a>
            </label>
            <button id={time.id}
                    onClick={this.handleDelete.bind(this)}
                    className="request-time-delete is-small is-pulled-right delete"></button>
          </p>
        </div>
      )
    });
  }
  onsubmit() {
    console.log("Form Submited...");
  }

  handleSelect({ target: { id } }) {
    this.props.setRequestTimeChoice('mandalay', id);
  }

  handleDelete({ target: { id } }) {
    this.props.deleteRequestTime('mandalay', id)
  }

}


// MAP TO PROPS
function mapStateToProps(state) {
  return {
    requestOptions: state.requestOptions
  }
}

// Redux connect
ViewRequestTimes = connect(mapStateToProps, {
  setRequestTimeChoice,
  deleteRequestTime
})(ViewRequestTimes);

export default ViewRequestTimes;
