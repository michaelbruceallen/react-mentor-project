// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import { deleteShiftQuery } from '../../actions';
import { prioritizeShiftQuery } from '../../actions';


class ViewShiftQueries extends Component {

  handlePrioritize(queryId) {
    this.props.prioritizeShiftQuery('mandalay', queryId);
  }

  handleDelete(queryId) {
    this.props.deleteShiftQuery('mandalay', queryId);
  }

  renderShiftQueries() {
    const { shiftQueryList } = this.props;

    if ( shiftQueryList.length === 0 ) {
      return (
        <div className="no-data">You should save some data...</div>
      )
    }

    return shiftQueryList.map((shiftQuery) => {
      let {id, type, date, startTime, endTime} = shiftQuery;
      if (!startTime)
        startTime = '-Any-';
      if (!endTime)
        endTime = '-Any-';
      return (
        <section key={id} className="panel-block">

          <a className="icon is-small is-left">
            <i onClick={() => this.handlePrioritize(id)} className="fa fa-arrow-up"></i>
          </a>

          <div className="shift-query control">
            <span className="type">{type}
            </span>
            <span className="date">{date}
            </span>
            <span className="start-time">{startTime}
            </span>
            <span className="end-time">{endTime}</span>
          </div>

          <a className="icon is-small is-right">
            <i onClick={() => this.handleDelete(id)} className="fa fa-trash"></i>
          </a>

        </section>
      )
    })
  }

  render() {
    return (
      <aside className="view-shift-queries">
        <section className="panel">
          {this.renderShiftQueries()}
        </section>
      </aside>
    )
  }
}

function mapStateToProps(state) {
  return { shiftQueryList: state.shiftQueryList }
}

// Redux connect
ViewShiftQueries = connect(
  mapStateToProps, {
    deleteShiftQuery,
    prioritizeShiftQuery,
  }
)(ViewShiftQueries);

export default ViewShiftQueries;
