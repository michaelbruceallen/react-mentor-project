// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import { getShiftQueryList, getRequestOptionData } from '../../actions';

// Components
import ViewShiftQueries from './ViewShiftQueries';
import AddShiftQuery from './AddShiftQuery';
import AddRequestTime from './AddRequestTime';
import ViewRequestTimes from './ViewRequestTimes';


class QueryOptions extends Component {

  componentDidMount() {
    this.props.getShiftQueryList('mandalay');
    this.props.getRequestOptionData('mandalay');
  }

  render() {
    return (
      <div className="options-display">
          <AddShiftQuery />
          <ViewShiftQueries />
          <AddRequestTime />
          <ViewRequestTimes />
      </div>
    )
  }
}


QueryOptions = connect(null, {
  getShiftQueryList,
  getRequestOptionData
})(QueryOptions);

export default QueryOptions;
