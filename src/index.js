// Modules
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

// Components
import MainMenu from './components/MainMenu';

// Reducer
import rootReducer from './reducers';


const myStore = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENTION__ && window.__REDUX_DEVTOOLS_EXTENTION__()
)

// Render React app to DOM
ReactDOM.render(
  <Provider store={myStore}>
    <MainMenu />
  </Provider>,
  document.getElementById('react')
)
