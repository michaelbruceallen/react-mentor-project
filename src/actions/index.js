// Types
import { PRIORITIZE_SHIFT_QUERY } from './types';
import { SET_REQUEST_TIME_CHOICE } from './types';
import { ADD_REQUEST_TIME } from './types';
import { ADD_SHIFT_QUERY } from './types';
import { GET_REQUEST_TIME_DATA } from './types';
import { GET_REQUEST_OPTION_DATA } from './types';
import { GET_SHIFT_QUERY_LIST } from './types';
import { DELETE_REQUEST_TIME } from './types';
import { DELETE_SHIFT_QUERY } from './types';

// Utils
import * as Utils from '../utils/StorageUtils';


export function setRequestTimeChoice(siteId, idToSet) {
    Utils.setRequestTimeChoiceById(siteId, idToSet);
    return {
      type: SET_REQUEST_TIME_CHOICE,
      payload: Utils.getRequestOptionDataById(siteId)
    }
}

export function deleteRequestTime(siteId, idToDelete) {
  Utils.deleteRequestTimeById(siteId, idToDelete);
  return {
    type: DELETE_REQUEST_TIME,
    payload: Utils.getRequestOptionDataById(siteId)
  }
}

export function addRequestTime(siteId, requestTime) {
  Utils.addRequestTimeById(siteId, requestTime);
  return {
    type: ADD_REQUEST_TIME,
    payload: Utils.getRequestOptionDataById(siteId)
  }
}

export function getRequestOptionData(siteId) {
  return {
    type: GET_REQUEST_OPTION_DATA,
    payload: Utils.getRequestOptionDataById(siteId)
  }
}

export function prioritizeShiftQuery(siteId, queryId) {
  Utils.prioritizeShiftQueryById(siteId, queryId);
  return {
    type: PRIORITIZE_SHIFT_QUERY,
    payload: getShiftQueries(siteId)
  }
}

export function getShiftQueryList(siteId) {
  return {
    type: GET_SHIFT_QUERY_LIST,
    payload: getShiftQueries(siteId)
  }
}

export function addShiftQuery(siteId, values) {
  values.id = Utils.generateUID();
  Utils.addShiftQueryById(siteId, values);
  return {
    type: ADD_SHIFT_QUERY,
    payload: getShiftQueries(siteId)
  }
}

export function deleteShiftQuery(siteId, queryId) {
  Utils.deleteShiftQueryById(siteId, queryId);
  return {
    type: DELETE_SHIFT_QUERY,
    payload: getShiftQueries(siteId)
  }
}

function getShiftQueries(siteId) {
  return Utils.getShiftQueryListById(siteId) || [];
}
