// Default workspace name
const APP_DATA = 'schedule_app_data';

// setup workspace object;
export function getAppData() {
  return JSON.parse(localStorage.getItem(APP_DATA)) || {};
}



//
// SHIFT QUERYS
//

// ADD SHIFT QUERY
export function addShiftQueryById(siteId, shiftQueryToSave) {
  const workspace = getAppData();
  const { shiftQueries } = workspace[siteId];
  // Create UID
  shiftQueryToSave.id = generateUID();
  // Save and update
  workspace[siteId].shiftQueries.push(shiftQueryToSave);
  updateWorkspace(workspace);
}

// GET SHIFT QUERY LIST
export function getShiftQueryListById(siteId) {

  let workspace = getAppData();
  if (!workspace[siteId]) {
    workspace = addSiteById(siteId);
  }
  const { shiftQueries } = workspace[siteId];
  return shiftQueries;
}

// PRIORITZE SHIFT QUERY
export function prioritizeShiftQueryById(siteId, idToPrioritize) {
  const appData = getAppData();
  const queryList = getShiftQueryListById(siteId);

  queryList.forEach((shift, index) => {
    if (shift.id === idToPrioritize && index > 0) {
      let temp = queryList[index-1];
      queryList[index-1] = queryList[index];
      queryList[index] = temp;
    }
  });

  appData[siteId].shiftQueries = queryList;
  updateWorkspace(appData);
}

// DELETE SHIFT QUERY
export function deleteShiftQueryById(siteId, idToDelete) {
  const workspace = getAppData();

  if(!workspace[siteId]) return null;

  const filteredShifts = workspace[siteId].shiftQueries
    .filter( shift => shift.id !== idToDelete );

  workspace[siteId].shiftQueries = filteredShifts;
  updateWorkspace(workspace);
}






//
// REQUEST TIMES
//

// DELETE REQUEST TIME
export function deleteRequestTimeById(siteId, idToDelete) {

  const workspace = getAppData();
  if(!workspace[siteId]) return null;
  const { times } = getRequestOptionDataById(siteId);
  const filteredTimes = times.filter( time => time.id !== idToDelete)
  workspace[siteId].requestOptions.times = filteredTimes;
  updateWorkspace(workspace);
}

// ADD REQUEST TIME
export function addRequestTimeById(siteId, time) {
  // Base workspace
  let workspace = getAppData();
  // generateUID
  const timeObj = { id: generateUID(), time}
  // Push timeObj to workspace
  workspace[siteId].requestOptions.times.push(timeObj);
  // Save to workspace
  updateWorkspace(workspace);
}

// GET REQUEST OPTION DATA
export function getRequestOptionDataById(siteId) {
  const workspace = getSiteById(siteId);
  return workspace.requestOptions;
}

// GET REQUEST TIME LIST
export function getRequestTimesById(siteId) {
  const workspace = getSiteById(siteId);
  return workspace.requestOptions.times;
}

// SET REQUEST TIME CHOICE
export function setRequestTimeChoiceById(siteId, choiceId) {
  if (!choiceId) return;
  const workspace = getAppData();
  workspace[siteId].requestOptions.choice = choiceId;
  updateWorkspace(workspace);
}





//
// SITE
//

// GET SITE
export function getSiteById(siteId) {
  const workspace = getAppData();
  if (!workspace[siteId])
    return addSiteById(siteId);
  return workspace[siteId];
}

// ADD SITE
export function addSiteById(name) {
  let workspace = getAppData();
  workspace[name] = {
    shiftQueries: [],
    requestOptions: { times: [], choice: 0 },
    logs: {}
  }
  localStorage.setItem(APP_DATA, asJSON(workspace) );
  workspace = getAppData();
  return workspace;
}

// DELETE SITE
export function deleteSiteById(siteId) {
  const workspace = getAppData();

  // Delete and update workspace
  delete workspace[siteId];
  updateWorkspace(workspace);
}




// Obj to JSON
function asJSON(data) {
  return JSON.stringify(data);
}
// Json to Obj
function asObj(json) {
  return JSON.parse(json);
}

// Update entire save file
function updateWorkspace(workspaceObj) {
  // Covert to JSON and save to localStorage
  localStorage.setItem(
    APP_DATA,
    JSON.stringify(workspaceObj)
  );
}

// UID generator
export function generateUID() {
  // I generate the UID from two parts here
  // to ensure the random number provide enough bits.
  var firstPart = (Math.random() * 46656) | 0;
  var secondPart = (Math.random() * 46656) | 0;
  firstPart = ("000" + firstPart.toString(36)).slice(-3);
  secondPart = ("000" + secondPart.toString(36)).slice(-3);
  return firstPart + secondPart;
}
